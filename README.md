# TDA (Trusted Digital Agent) Project Summary

For more information, please contact: 

**Project SPOC:** 
Thomas Bonefeld Jørgensen: [thomas@unikk.me](thomas@unikk.me)

**Project core team:**
Lotta Lundin: [lotta@igrant.io](lotta@igrant.io)
Lal Chandran: [lal@igrant.io](lal@igrant.io)
Jan Lindqvist: [jan@linaltec.com](jan@linaltec.com)
George Padayatti: [george@igrant.io](george@igrant.io)
Selbin: [selbin@igrant.io](selbin@igrant.io)   

## Table of contents
- [Introduction](#introduction)
  * [About Trusted Digital Agent (TDA)](#about-trusted-digital-agent-tda)
  * [Trusted Digital Agent (TDA)](#trusted-digital-agent-tda)
- [Summary](#summary)
- [Business Problem](#business-problem)
  * [Problem Definition](#problem-definition)
  * [Business Solution and Stakeholders](#business-solution-and-stakeholders)
- [Technical Solution](#technical-solution)
  * [Reference Implementation](#reference-implementation)
  * [Data Source and Data Using Services](#data-source-and-data-using-services)
  * [Overall Architecture](#overall-architecture)
  * [Software Components View](#software-components-view)
  * [Concepts and Process Workflows](#concepts-and-process-workflows)
    + [Process of Preating Verifiable Credential](#process-of-preating-verifiable-credential)
    + [Process for Performing Proof Request](#process-for-performing-proof-request)
  * [Data Agreements](#data-agreements)
    + [Privacy Agreement](#privacy-agreement)
    + [Code of Practice](#code-of-practice)
- [Integration with the eSSIF-Lab Functional Architecture and Community](#integration-with-the-essif-lab-functional-architecture-and-community)
  * [Summary of project deliverable](#summary-of-project-deliverable)
  * [Overview of the APIs](#overview-of-the-apis)
  * [Verifiable Credential work-flows and APIs](#verifiable-credential-work-flows-and-apis)
    + [Prepare public schema](#prepare-public-schema)
      - [Create DID in a wallet](#create-did-in-a-wallet)
      - [Create schema definition](#create-schema-definition)
    + [Establish connection between Issuer and Holder](#establish-connection-between-issuer-and-holder)
    + [Negotiate verifiable credential](#negotiate-verifiable-credential)
  * [Verifier/Proof work-flows and APIs](#verifierproof-work-flows-and-apis)
    + [Establish connection between Verifier and Holder](#establish-connection-between-verifier-and-holder)
    + [Automated Holder response](#automated-holder-response)
    + [Manual input from Holder](#manual-input-from-holder)
- [References](#references)

## Introduction

### About Trusted Digital Agent (TDA)

The TDA or the Trusted Digital Agent project is a concept developed by **[unikk.me aps](www.unikk.me)** (Denmark) and **[iGrant.io](www.igrant.io)** (Sweden). 

### Trusted Digital Agent (TDA)

The purpose of the project is to design and develop ready-to-use services and toolkits for any organisation to transform their application to a TDA. The toolkit includes, but is not limited to, mobile Software Development Kits (SDKs), extending the current hyperledger Indy/Aries software with a consent lifecycle. With the TDA add-on, the organisation can become a trusted entity while continuing to provide advanced user experience. We reuse the data operator service stack as described by MyData Operaror (iGrant.io) providing various services required for a TDA to offer a fully-fledged, data regulatory compliant, service. 

Once the base infrastructure is created, a human centric services will be provided, under the governance of the TDA, by a third party. We demonstrate the overall solution using a health-data wallet app called Data4Life. 

## Summary

<!--Provide a *concise* description of your subproject, that helps other subgrantees to quickly determine what value there is if they work together with you, and/or let their results interact and interop with your results.-->
Every individual has the fundamental right to control their data rights including their identity. This project enables parties to transform their applications so they become trusted digital agents (TDAs) catering for safety and protection, convenience and benefits for every individual and organisation.

The project aims to transform any organisation’s application(s) through the use of Self-Sovereign Identity (SSI) and other data operator services. The SSI technology provides real-time data exchange capabilities for the individual’s Data Wallet provided by the TDA, using data from multiple Data Sources. The SSI brings in decentralized application capabilities within the Data Operator framework (as defined by MyData). This enables the individual to be a Data Source and exchange consented personal data from their wallet with a Data Using Service as per the MyData governance framework. The Data Using Service can verify this data authenticity independently as well. 

In this project, we design and develop ready-to-use services and toolkits for any organisation to transform their application to a TDA. The toolkit includes, but is not limited to, mobile Software Development Kits (SDKs), extending the current hyperledger Indy/Aries software with a consent lifecycle. With the TDA add-on, the organisation can become a trusted entity while continuing to provide advanced user experience. We reuse the data operator service stack as described by MyData providing various services required for a TDA to offer a fully-fledged, data regulatory compliant, service. The solution also supports human centric services provided, under the governance of the TDA, by a third party. We demonstrate the overall solution using a health-data wallet app called Data4Life. 

Our target contribution to eSSIF-Lab is to reduce the barriers for developers (and the businesses they serve) via a reference implementation and advocate standards that be developed and adopted by various stakeholders. Thus, the SDKs, the reference implementation and the consent lifecycle design will be  key assets for  eSSIF-Lab stakeholders and subgrantees.

## Business Problem

### Problem Definition
<!--Provide a *concise* description of the business problem you focus on. What is the problem? Who needs to deal with it/suffers drawbacks?-->
Fundamentally, there is a lack of trust between the parties interacting on the Internet. First priority in resolving this problem is identification: when A knows who B is, a mutual evaluation can take place to establish trust or reject interaction. In order to solve the problem around lack of trust and need for identification both commercial businesses and public authorities are offering solutions for individuals as well as for organizations.

A large number of commercial solutions have led to the Internet as we know it where interactions are centered around trading goods and services, and even trading ‘human attention’, i.e. realizing the phrase of “if the service is free, then you are the product”. In the evolution of the Internet expanding on personal data, we have even reached the point where “You are not the product; you are the abandoned carcass”. The governmental solutions are either pseudo independent of commercial interest, lagging behind or, worse, absent for ¾ of the world population. So with the current Internet we are stuck with the problem of lack of trust between parties interacting. Hence, we need to solve that problem first, before solving the problem of exchanging data in a privacy preserving way. In brief:

We need to address the **lack of trust** **(Problem 1)** and **inability to unlock the value of personal data** **(Problem 2)** 

For the **individuals** this means:

1. Lack of transparency as to how an individual’s data is collected, used and shared across organisations and lack of empowerment to be in control of the data

2. Siloed data (residing with a particular organisation) means low or no value of data to the person and do not enable data reuse or repurposing

For **organisations**, this means:

3. Risk associated with collecting, consuming, exchanging and storing sensitive person data in an ethical and secure way

4. Fear of non compliance to data regulations in the use of personal data

For individuals, solving problem 1 and 2 while being dependent on a private or public organisation, may give rise to additional concerns of surveillance and lock-in (Problem 3). Such concerns are addressed by providing seamless ability to exchange data via an NGO TDA with the sole raison d´être to be an extension of the individuals’ personal wallet.

### Business Solution and Stakeholders

This section elaborates on the different business solutions associated with solving each of the problems 1, 2 and 3. The picture below illustrates the stakeholders involved to deliver the end-to-end service to the end-user. 
![](images/BusinessSolution.png)

Trusted Digital Agents (TDAs) can be offered by many different organisations, both public or private, to provide a full service or merely parts of a data operator framework  (c.f. the reference model as defined in the MyData Global white paper [4]) and the service can have a commercial or altruistic purpose. The above is an attempt to capture the alternative TDA set-ups and the key functions needed to provide the service. The actors and key functional elements are described further below and additional details are provided in the [Technical Solution](#technical-solution) section. 

### The Trusted Digital Agent

An organisation (the data controller in the context of GDPR) with the ambition to create a trust framework between the individual and organisation(s), data sources and data using services, by providing an application that serves as a TDA, thus addressing Problem 1 (trust) and Problem 2 (unlocking value).

The TDA offers coherent end-user services centered around SSI, PDS (Personal DataSstore) and data exchange whereby individuals are empowered to control their data. Consented data is exposed to data using services as outlined in Section: Technical Solution).

The TDA will fund its business through one or more of the following sources: membership fees, governmental or EU programmes, revenue from selling donated data and public or private grants (soft-money). The services will be provided by the organisation behind the TDA or by a third party (cf. below human centric services) .

### TDA SDKs 

The TDA SDK is the mobile agent code that is created as part of a hackathon. Any application wishing to convert their existing apps as a TDA can incorporate this and convert their app to a TDA: The TDA SDK is released as open source.  

### The Data Operator Platform   

As defined in the introductory MyData Operator whitepaper [Ref. 4], a Data Operator provides infrastructure for human-centric personal data management and governance, here referred to as the Data Operator Platform. The platform consists of nine core functional elements. These elements affect how easy it is to utilise personal data, how transparent and human-centric the utilisation of personal data is, and how well the infrastructure supports open competition. 

The platform enables a TDA to incorporate all or parts of the core data operator elements in their applications as well as governance systems. The platform element “Value Exchange” facilitates accounting and captures value (monetary or other forms of credits or reputation) created in the exchange of data. The solution hence enables all actors in the ecosystem to monetize the personal data asset, with the permission of the individual.

iGrant.io is a MyData awarded data operator with the ambition to enable also other data operators and trusted intermediaries.

### Human-centric Service 

While we today are faced by our data value in the shape of personified ads and other transformations, the decentralized data economy will give rise to more refined services to the individuals, and a closer customer relation for the organisation using their data to create more meaningful services.

In this era, and while assuming adoption of SSI, the providers of technology enabling human centric services, addressing problem 3 (surveillance), will be key players. These players will offer micro services that enable greenfield (sharing-economy) and legacy service providers (gyms, dietists, physiotherapists) to enhance their services digitally. 

Unikk.me has the ambition to be the leading provider of human centric services on behalf of the TDAs to providers of performance health services.

### Data Sources and Data Using Services

The role responsible for collecting, storing, and controlling personal data which persons, operators, and data using services may wish to access and use. Here, the TDA can be both the data source and the data using services and also can involve other data sources and other data using services.

### Ecosystem Enablers

These are additional entities that ensure governance, platform developers, standardisation bodies, data trusts and similar trusted intermediaries that act together to enable a human-centric, trusted data exchange. 


## Technical Solution

<!--Provide a *concise* list of the (concrete) results you will be providing (include a summary description of each of them), and how they (contribute to) solve the business problem(s)-->

### Reference Implementation

In our reference implementation, illustrated below, we have the individual (Alice) holding her health data in her third party wallet app issued by Data4Life. This health data can be used by Alice to prove her health status to a travel company. The Data Operator acts in line the schema defined by a data governance authority (Referred in RFC-167 [Ref. 1] as the “Legal Entity”) to ensure that the test center follows the schema and governance as well as maintaining the agreements (permissions, consents and agreements). 

![Reference Implementation](images/ReferenceSolution.png)

In this solution we use a hyperledger indy based decentralised ledger, available at [https://indy.igrant.io/](https://indy.igrant.io/). More details are provided in the second delivery (eSSIF-Lab: Trusted Digital Assistant -  Contributions to eSSIFLab and SSI). The actors, their roles are also covered in detail in that deliverable. 

### Data Source and Data Using Services

In our reference implementation, Data4Life is the Trusted Digital Agent (TDA) where it acts as the individual wallet. It also handles the information sharing agreements and consents. The different Data Sources and the Data Using Service involved in the reference implementation and the mechanism used for data exchange and the agreement types are summarised in the table below.

| Data Source                             | Data Using Services | What Data?             | Agreement Type / Consent                                                                                                                                                                                                                                                       |
|-----------------------------------------|---------------------|------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Test Center                             | Data4Life           | TestResult, TestDate | Consent required for the Test Center to share their data with Data4Life.                                                                                                                                                                                                       |
| Data4Life (Wallet)                      | Any 3pp             | TestResult, TestDate | Data4Life has the consent to receive the data and use it towards any 3PP Data Using Service Privacy Agreement with Data4Life                                                                                                                                                   |
| Data4Life (Data Source from the wallet) | Travel Company      | TestResult, TestDate | Consent to data services that can be provided based on the stored certificates and data stored in the individuals wallet Travel Company may need an agreement with the Test Center. A receipt is generated when the data is exchanged, in some cases it could be consent. |

### Overall Architecture

A brief overview of the system architecture behind **the reference solution,  Data4Life** is as shown below. It shows the existing services and the components that will be developed as part of the hackathon project.

![Overall SW Architecture](images/Overallarchiecture_Data4Life.png)

The work in this project will focus on enhancing identity management with SSI based solutions. The code will rely on existing Aries components (open sourced) and we will develop new APIs that will connect permissions and consents with SSI based identity. Any TDA (such as Data4Life, as an example) will be able to use the microservices and package the client SDKs (aka TDA project results) into their existing solutions.

The key functions we intend to implement as part of this hackathon are:

1. Be able to hold third party data that can be carried by the individual in their wallet for an individual to carry third party data in their data wallet
2. Exchange the data, actively (vs passively),  in a privacy centric manner with individual user in control of the data, compliant to GDPR, both actively and passively
3. For an organisation being able to independently verify the authenticity of the data that the individual has shared
4. To ensure tamper proof receipts extended with user keys for privacy agreements for privacy agreements, permissions and consented data exchange (or any information sharing agreements). 
5. Develop websocket based developer tooling to debug agents during development and even later

The technical components that will be developed and/or enhanced as part of this project (published at:  [https://github.com/decentralised-dataexchange](https://github.com/decentralised-dataexchange)) include the following:

1. Dockerised DID and VC components that can be readily used by any developer to get the entire DID/VC Trusted data agent suite added to their existing data operator solution
2. Combined APIs that links a data exchange to consent APIs used for online and offline purposes. These new APIs are published along with existing iGrant.io APIs
3. Mobile agent in Dart (towards iOS and android): Aries mobile agent providing an easy way for any app developers to build decentralized identity services that use verifiable credentials
4. Organisation SDK that extends Aries cloud agent that makes integration to an existing organisations IT infrastructure easy

The following subsections represent the type of agreements that can be set up. The first one is privacy agreement. The table represents the actors involved when setting up the agreement, their role from SSI perspective and a basic description of the actions involved.

### Software Components View

The following diagram shows the different platform layers that will be run to support the reference solution:

![Software Components View](images/SWComponents.png)

### Concepts and Process Workflows

#### Process of Preating Verifiable Credential
The following diagram is a copy of the consent lifecycle for creating a verifiable credential as per Aries RFC 167 (Ref [1]). The actors from the reference implementation section are mapped to the diagram.

![Process of creating Verifiable Credential](images/ChoreographyDiagram_01.png)
<!--<center><img src="images/ChoreographyDiagram_01.png" alt="Process of creating Verifiable Credential" width="750" height="600" /></center>
-->
#### Process for Performing Proof Request
The following diagram is a copy of the consent lifecycle for performing a proof request as per Aries RFC 167 (Ref [1]). The actors from the reference implementation section are mapped to the diagram.

<!--<center><img src="images/ChoreographyDiagram_02.png" alt="Process for performing Proof Request" width="750" height="500" /></center>
-->
![Process for performing Proof Request](images/ChoreographyDiagram_02.png)
### Data Agreements
#### Privacy Agreement
In our reference implementation, a privacy agreement is set up between the test center and the individual. Also known as Consent Notice (Kantara) or Privacy Notice (MyData). In the hackathon stage, the intention is to reuse the existing permissions and consents APIs, service management APIs etc in iGrant.io (The Data Operator) as-is with the option to, later, replace them with any third party data operator Code of Conduct or Data Sharing Agreements.

The Code of Conduct (Kantara) or Data Sharing Agreement (MyData) is set up between organisations  by 3rd parties like research groups. 

#### Code of Practice 
The code or practice is a statement of the adherence to GA4GH (Ref. [5]). This is equivalent to privacy by design but more accountable and transparent. 

## Integration with the eSSIF-Lab Functional Architecture and Community

### Summary of project deliverable

| Item                           | Release link                                 |  
| :------------------------------| :----------------------------------------------| 
| Open source code GitHub Repo:  | https://github.com/decentralised-dataexchange  |
| SSI user guide and docs        | https://docs.igrant.io/ssi/                    |  
| Aries sandbox for interop testing  | https://docs.igrant.io/ssi/ssi-apg      | 
| Aries mobile agent w/ supported RFC info (WIP)  | https://docs.igrant.io/ssi/ssi-ama     |
| SSI lifecycle with data agreements  | https://docs.igrant.io/ssi/ssi-lifecycle  |

### Overview of the APIs

The **main aries agent + additional APIs** used are available as swagger spec at: https://demo-aries-agent-admin.igrant.io/api/doc

The below sub-sections summarise the main APIs used with instrutions to try it out. All MyData Operator apis are published at: https://developer.igrant.io/ 

### Verifiable Credential work-flows and APIs

The work flow shows 4 steps when creating a verifiable credential (or agreement) as depicted in below diagram. 

1. **[Prepare public schema](#prepare-public-schema)** including registering issuer DID
2. **[Establish connection between Issuer and Holder](#establish-connection-between-issuer-and-holder)** (Alice)
3. **[Issue verifiable credential](#issue-verifiable-credential)** represented as an agreement between Holder and Issuer
4. **[Negotiate verifiable credential](#negotiate-verifiable-credential)** with agreement offer

The last step (4) allows the Holder (Alice) to review the offer from the Issuer before accept it. Note step 3 is automated to skip the negotiation. In order to allow to negotiate The file [startup.sh](https://github.com/decentralised-dataexchange/aries-playground/blob/master/cloud-agent/startup.sh) in folder *aries-playground/cloud/agent* needs to be renamed for [startup_temp.sh](https://github.com/decentralised-dataexchange/aries-playground/blob/master/cloud-agent/startup_temp.sh).

![](images/ssi-agreement-flow-detailed-boxes.svg)

For further details on the diagram refer to [Hyperledger Aries RFC 0167 Consent Lifecycle](https://github.com/hyperledger/aries-rfcs/blob/master/concepts/0167-data-consent-lifecycle/README.md)

#### Prepare public schema

Before creating schema it is necessary to create a DID.

##### Create DID in a wallet

The steps below to create DID in a wallet is a pre-requesite for any agent before acquiring agent roles.

1. Create a local DID for the agent using `POST ​/wallet​/did​/create`. This generates the DID and verification key [localhost endpoint](http://test-center.swagger.localhost/api/doc#/wallet/create) 
	
	Response body
	
  ```json
  {
	  "result": {
		"did": "TqQDq6vrPoW7hsf4JvqPaR",
		"verkey": "FdHtGAaod2xoZ7wJfXU4yCEEuTt6hSf38EpdKkX4UHgK",
		"posture": "wallet_only"
	  }
  }
  ```

To make the DID public use `POST ​/wallet​/did​/public`. Input the DID from previous response. This step is required for creating schema. ([localhost endpoint](http://test-center.swagger.localhost/api/doc#/wallet/public))
	
2. For organisations, after creating the local DID, you need to register it with ledger at [indy.igrant.io](https://indy.igrant.io/) as shown below with the DID and verification key

![](images/ssi-indy-screenshot.svg)

After registering with the Indy ledger call `POST ​/wallet​/did​/public`([localhost endpoint](http://test-center.swagger.localhost/api/doc#/wallet/public)) 

**Optional possibility to configure other ledgers**: To regiseter with **Sovrin Staging** network [use this Sovrin Indy network link](https://selfserve.sovrin.org) and complete details as shown in diagram. Important that docker-compose.yml has been updated prior to starting docker-compose.

![](images/ssi-sovrin-screenshot.png)

##### Create Schema

To make it easier, we have used the [Test Center Agent](http://test-center.swagger.localhost) to register the schema. Ideally this is defined by a legal entity or a standardisation body.

You can execute the schema definiton API to register the schema in the ledger.([localhost endpoint](http://test-center.swagger.localhost/api/doc#/wallet/schema))

Sends a schema to the ledger with the API `POST: ​/schemas` with the json body as given:

```json  
{
  "schema_version": "1.0",
   "schema_name": "Covid-19 Test Results",
   "attributes": [
     		"testResult",
     		"testDate"
  		]	
 }
```

Response body

```json
{
  "schema_id": "3jbQ51y4RwCb4U45rkeKY1:2:Covid-19 Test Results:1.0",
  "schema": {
    "ver": "1.0",
    "id": "3jbQ51y4RwCb4U45rkeKY1:2:Covid-19 Test Results:1.0",
    "name": "Covid-19 Test Results",
    "version": "1.0",
    "attrNames": [
      "testResult",
      "testDate"
    ],
    "seqNo": 370
  }
}
```

##### Create schema definition

Prior to making any verifiable credential a schema definition has to be created which is means of performing proofs against the credential. `POST method /credential-definitions` stores a credential definition against the ledger. ([localhost endpoint](http://test-center.swagger.localhost/api/doc#/credential-definition/post_credential_definitions))

Add following body to the request.

```json
{
  "revocation_registry_size": 4,	
  "support_revocation": false, "schema_id": "PWr9PACurgoMwowC5Bx8RD:2:Covid-19 Test Results:1.0",
  "tag": "default"
}
```

Response body

```json
{
  "credential_definition_id": "3jbQ51y4RwCb4U45rkeKY1:3:CL:370:default"
}
```

Ready to issue verifiable credentials.

#### Establish connection between Issuer and Holder
 
Here, the Test Center and Data4Life-User agents establishes connection with each other. The following is the API call sequence:

1. Create a new invitation (by Test Center)

	Test Center Agent: `POST ​/connections​/create-invitation`. Use default values.([localhost endpoint](http://test-center.swagger.localhost/api/doc#/connection/post_connections_create_invitation))
	
	This generates the `connection_id` and `invitation`.
	
  Response body

	```json  
    {
      "connection_id": "521f106f-b10e-472a-b7ad-219a812e31e1",
      "invitation": {
        "@type": "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/connections/1.0/invitation",
        "@id": "c3055fe4-9bca-4585-918c-67bbdb07bc10",
        "label": "Test-Center",
        "serviceEndpoint": "http://test-center.localhost",
        "recipientKeys": [
          "7myawP9fdA1y237bi3Xdfukuv2rX8YRNzt2s2xEdpJh3"
        ]
      },
      "invitation_url": "http://test-center.localhost?c_i=eyJAdHlwZSI6ICJkaWQ6c292OkJ6Q2JzTlloTXJqSGlxWkRUVUFTSGc7c3BlYy9jb25uZWN0aW9ucy8xLjAvaW52aXRhdGlvbiIsICJAaWQiOiAiYzMwNTVmZTQtOWJjYS00NTg1LTkxOGMtNjdiYmRiMDdiYzEwIiwgImxhYmVsIjogIlRlc3QtQ2VudGVyIiwgInNlcnZpY2VFbmRwb2ludCI6ICJodHRwOi8vdGVzdC1jZW50ZXIubG9jYWxob3N0IiwgInJlY2lwaWVudEtleXMiOiBbIjdteWF3UDlmZEExeTIzN2JpM1hkZnVrdXYyclg4WVJOenQyczJ4RWRwSmgzIl19"
    }
	```	

2.	Receive connection invitation by Data4Life-User (Alice)

	Alice Agent: `POST ​/connections​/receive-invitation` with the invitation extracted from the json body generated in step 1. ([localhost endpoint](http://data4life-user.swagger.localhost/api/doc#/connection/post_connections_receive_invitation))
	
  Response body

	```json
	{
      "connection_id": "54b9fdd1-99b4-4ecf-a388-5cb04ebffa9f",
      "initiator": "external",
      "invitation_key": "7myawP9fdA1y237bi3Xdfukuv2rX8YRNzt2s2xEdpJh3",
      "state": "invitation",
      "created_at": "2020-12-23 15:49:24.936970Z",
      "routing_state": "none",
      "invitation_mode": "once",
      "their_label": "Test-Center",
      "updated_at": "2020-12-23 15:49:24.936970Z",
      "accept": "manual"
    }

	```

3. Accept a received connection invitation by Data4Life-user (Alice)

	Alice Agent: `POST ​/connections​/{conn_id}​/accept-invitation` passing the `connection_id` as input. ([localhost endpoint](http://data4life-user.swagger.localhost/api/doc#/connection/post_connections__conn_id__accept_invitation))

  Response body
	
  ```json
    {
      "connection_id": "54b9fdd1-99b4-4ecf-a388-5cb04ebffa9f",
      "initiator": "external",
      "invitation_key": "7myawP9fdA1y237bi3Xdfukuv2rX8YRNzt2s2xEdpJh3",
      "state": "request",
      "created_at": "2020-12-23 15:49:24.936970Z",
      "request_id": "2bbcb177-83b1-4f6a-819c-d3b585c2b80d",
      "routing_state": "none",
      "my_did": "TiXWip9b6KCYdmfa4MJRng",
      "invitation_mode": "once",
      "their_label": "Test-Center",
      "updated_at": "2020-12-23 15:55:26.730952Z",
      "accept": "manual"
    } 
   ```

4. Check status

	Check both Test Center Agent and Alice Agent by `GET /connections `and both are in `Active` status.
    
    Test Center agent status check ([localhost endpoint](http://test-center.swagger.localhost/api/doc#/connection/get_connections))
    
    Alice agent status check ([localhost endpoint](http://data4life-user.swagger.localhost/api/doc#/connection/get_connections))

After the secured connection is established between the two agents, the Test Center first establishes the connection with Alice. After that the Test Center issues credential to them with their own personal data. Alice then is able to see the credential in her Data4Life wallet.

#### Issue verifiable credential

A credential is issued by the Test Center based on a standard schema earlier defined by the legal entity. Test Center now issues the credential to the holder Alice (Data4Life-user).
	
Test Center Agent: `POST ​/issue-credential​/send` with `auto_remove` set to FALSE. ([localhost endpoint](http://test-center.swagger.localhost/api/doc#/issue-credential/post_issue_credential_send))

```json
{ 
 "schema_name": "Covid-19 Test Results",
 "schema_version": "1.0",
 "cred_def_id": "PWr9PACurgoMwowC5Bx8RD:3:CL:19:default",
 "auto_remove": false,
 "comment": "string",
 "connection_id": "a8ea680f-0704-4327-99b8-02e5e3d03ea4",
 "trace": false,
 "schema_issuer_did": "PWr9PACurgoMwowC5Bx8RD",
 "schema_id": "PWr9PACurgoMwowC5Bx8RD:2:Covid-19 Test Results:1.0",
 "issuer_did": "PWr9PACurgoMwowC5Bx8RD",
 "credential_proposal": {
 "@type": "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/issue-credential/1.0/credential-preview",
 "attributes": [
	{
	  "name": "testDate",
	  "mime-type": "text/plain",
	  "value": "22-Aug-2020"
	},
	{
	  "name": "testResult",
	  "mime-type": "text/plain",
	  "value": "negative"
	}
  ]
 }
}
```

#### Negotiate verifiable credential

1. Issuer (Test Center) sends a offer with the result of the test.  

	Test Center Agent: `POST/issue-credential​/send-offer` ([localhost endpoint](http://test-center.swagger.localhost/api/doc#/issue-credential/post_issue_credential_send_offer))
		
	**Note:** In request body auto issue and auto remove should be false  
	
	```json
	{
	  "cred_def_id": "WgWxqztrNooG92RXvxSTWv:3:CL:20:tag",
	  "credential_preview": {
		"@type": "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/issue-credential/1.0/credential-preview",
		"attributes": [
		{
		  "name": "testDate",
		  "mime-type": "text/plain",
		  "value": "22-Aug-2020"
		},
		{
		  "name": "testResult",
		  "mime-type": "text/plain",
		  "value": "negative"
		}
		]
	  },
	  "connection_id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
	  "auto_issue": false,
	  "comment": "string",
	  "trace": false,
	  "auto_remove": false
	}
	```
		
	Now the state will be offer_sent for Issuer (Test Center)  
	   
	**Note:** `GET /issue-credential​/records` to view the state. ([localhost endpoint](http://test-center.swagger.localhost/api/doc#/issue-credential/get_issue_credential_records))

2. Alice will call `GET /issue-credential​/records`  

	The state will be offer_received and from this response copy the credential exchange id. ([localhost endpoint](http://data4life-user.swagger.localhost/api/doc#/issue-credential/get_issue_credential_records))

3. Alice will send request to issue the credential using credential exchange id

	Alice Agent: `POST /issue-credential​/records​/{cred_ex_id}​/send-request` ([localhost endpoint](http://data4life-user.swagger.localhost/api/doc#/issue-credential/post_issue_credential_records__cred_ex_id__send_request))
	
	Now the state will be request_sent for Alice

4. Issuer (Test Center) will call `GET/issue-credential​/records`  

	The state will be request_received and from this response copy the credential exchange id. ([localhost endpoint](http://test-center.swagger.localhost/api/doc#/issue-credential/get_issue_credential_records))

5. Issuer (Test Center) issues the certificate using credential exchange id  

	Test Center Agent: `POST /issue-credential​/records​/{cred_ex_id}​/issue` ([localhost endpoint](http://test-center.swagger.localhost/api/doc#/issue-credential/post_issue_credential_records__cred_ex_id__issue))
	
	Now, the credential will be issued to Alice and state will change to credential_issued   

6. Stores credential into a personal wallet

	Personal wallet here is provided by Data4Life app. In the case of automated flow, the credential is automatically stored into the wallet. In the case of manual flow, this need to be done explicitly.
	
	For manual flow, Alice, the Data4Life-User, now stores the received credentials
	
	Alice Agent: `POST ​/issue-credential​/records​/{cred_ex_id}​/store` ([localhost endpoint](http://data4life-user.swagger.localhost/api/doc#/issue-credential/post_issue_credential_records__cred_ex_id__store))
	
	Data4Life user can fetch the credentials from the wallet by `GET /credentials` 

### Verifier/Proof work-flows and APIs

This is from the Holder app (Data4Life) to Verifier (Travel Company)

Before any communication happens between Alice (Data4Life-User) and the verifier, a secured connection is established between two agents. After that Travel Company issues a proof request to Alice, showing what type of proof is needed to qualify in order for Alice to travel using the Covid-19 test result. Alice will build the proof based on the credential in her Data4Life wallet. Alice then sends the proof to the travel company which will observe the result. The proof response can be done automatically or through manual input. In order to run manual modify startup.sh file as described in section [Work flow](#work_flow).

The following diagram provides an overview of all the steps taking place under the hood. For further details refer to [Hyperledger Aries RFC 0167 Consent Lifecycle](https://github.com/hyperledger/aries-rfcs/blob/master/concepts/0167-data-consent-lifecycle/README.md)

![](images/ssi-proof-flow-boxes.svg)

The following sections are broken down into the following areas:

1. Establish connection between Verifier and Holder
2. Automated Holder response
3. Manual input from Holder

#### Establish connection between Verifier and Holder 

Establishign connection is identical to the flow in Issuer and holder as described in section [Establish connection between Issuer and Holder](#connect)

1. Establish connection

	Travel Company Agent: `POST /connections/create-invitation`, from the response get the invitation object (from `{ to }`) as shown earlier during the connection between Test Center and Alice. ([localhost endpoint](http://travel-company.swagger.localhost/api/doc#/connection/post_connections_create_invitation))

	Alice Agent: `POST /connections/receive-invitation` with the invitation object. ([localhost endpoint](http://data4life-user.swagger.localhost/api/doc#/connection/post_connections_receive_invitation ))

2. Accept a stored connection invitation by Alice (Data4Life user)

	Alice Agent: `POST ​/connections​/{conn_id}​/accept-invitation` passing the `connection_id` as input. ([localhost endpoint](http://data4life-user.swagger.localhost/api/doc#/connection/post_connections__conn_id__accept_invitation))
   
	Check both Travel Company Agent and Alice Agent by `GET /connections `and both are in `Active` status

	After the secured connection is established between the two agents, the Travel Center request a proof from Alice as per the credenital defintion. The following steps cover that flow.

#### Automated Holder response

In this demo, the Proof request details the Test Center is asking for is

	testResult
	testDate

The requested attributes follows the Credential Definition specified by the ID

Travel Center Agent: `POST /present-proof/send-request` is called with the following payload. ([localhost endpoint](http://travel-company.swagger.localhost/api/doc#/present-proof/post_present_proof_send_request))

The body in the request shall have the following details. Modify the `connection_id` and `cred_def_id`.

```json
{
  "connection_id": "a8ea680f-0704-4327-99b8-02e5e3d03ea4",
  "comment": "Ready to travel",
  "proof_request": {
  "name": "Proof of COVID19 Negative",
  "version": "1.0",
  "requested_attributes": {
  "0_testresult_uuid": {
	"name": "testResult",
	"restrictions": [
	  {
		"cred_def_id": "PWr9PACurgoMwowC5Bx8RD:3:CL:19:default"
	  }
	]
   },
   "0_testdate_uuid": {
	"name": "testDate",
	"restrictions": [
	  {
		"cred_def_id": "PWr9PACurgoMwowC5Bx8RD:3:CL:19:default"
	  }
	]
   }
 },
 "requested_predicates": {}
 }
} 
``` 

Using the requested_predicates, you can do some assertions, example, the testDate shall be less than 2 days from today. 

In the webhook interceptor, you can view the sequence of events happening:

* receiving the proof request
* checking credentials
* generating proof
* sending proof to Travel Company

From Travel Company, we can use `GET /present-proof/records` to see the proof sent by Alice. The `presentation_exchange_id` is the identifier of the presentation proof and state will tell you the current status of the presented proof. ([localhost endpoint](http://travel-company.swagger.localhost/api/doc#/present-proof/get_present_proof_records))

#### Manual input from Holder

Instead of an automatic response from the Holder the proof can be manually entered.

1. Holder (Alice) has to get the presentation exchange id  
   Alice Agent: `GET /present-proof/records` ([localhost endpoint](http://data4life-user.swagger.localhost/api/doc#/present-proof/get_present_proof_records))

2. Holder (Alice) will send the presentation
   
	Alice Agent: `POST ​/present-proof​/records​/{pres_ex_id}​/send-presentation` is called with the following payload. ([localhost endpoint](http://data4life-user.swagger.localhost/api/doc#/present-proof/post_present_proof_records\__pres_ex_id__send_presentation))

	The body in the request shall have the following details. The `pres_ex_id` is copied from previous step.

	```json
	{
	 "requested_predicates": {
	 },
	
	 "trace": false,
	 "self_attested_attributes": {
	 },
	
	 "requested_attributes": {
		"0_testresult_uuid": {
			"cred_id": "b5df5eac-047f-4ad0-98cc-7e6138a2f339",
			"revealed": true
		},
		"0_testdate_uuid": {
			"cred_id": "b5df5eac-047f-4ad0-98cc-7e6138a2f339", 
			"revealed": true
		}
	  }
	}
	```
	
	**Note:** requested_attribute fields keys should be same as in the body of  `POST /present-proof/send-request`   

3. Finally Travel Company use `POST /present-proof/{pres-ex-id}/verify-presentation` to see Alice’s proof presentation. ([localhost endpoint](http://travel-company.swagger.localhost/api/doc#/present-proof/post_present_proof_records__pres_ex_id__verify_presentation))

## References
1. [Setting up indy network and agents](https://github.com/decentralised-dataexchange/aries-playground/blob/master/README.md) 
2. [Hyperledger Aries RFC 0167 Consent Lifecycle](https://github.com/hyperledger/aries-rfcs/blob/master/concepts/0167-data-consent-lifecycle/README.md)

