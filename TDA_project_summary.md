# BOC-1 - Project Summary of Trusted Digital Agent <!--subproject name-->

<!--Provide a *concise* description of your subproject, that helps other subgrantees to quickly determine what value there is if they work together with you, and/or let their results interact and interop with your results.-->
Every individual has the fundamental right to control their data rights including their identity. This project enables parties to transform their applications so they become trusted digital agents (TDAs) catering for safety and protection, convenience and benefits for every individual and organisation.

The project aims to transform any organisation’s application(s) through the use of Self-Sovereign Identity (SSI) and other data operator services. The SSI technology provides real-time data exchange capabilities for the individual’s Data Wallet provided by the TDA, using data from multiple Data Sources. The SSI brings in decentralized application capabilities within the Data Operator framework (as defined by MyData). This enables the individual to be a Data Source and exchange consented personal data from their wallet with a Data Using Service as per the MyData governance framework. The Data Using Service can verify this data authenticity independently as well. 

In this project, we design and develop ready-to-use services and toolkits for any organisation to transform their application to a TDA. The toolkit includes, but is not limited to, mobile Software Development Kits (SDKs), extending the current hyperledger Indy/Aries software with a consent lifecycle. With the TDA add-on, the organisation can become a trusted entity while continuing to provide advanced user experience. We reuse the data operator service stack as described by MyData providing various services required for a TDA to offer a fully-fledged, data regulatory compliant, service. The solution also supports human centric services provided, under the governance of the TDA, by a third party. We demonstrate the overall solution using a health-data wallet app called Data4Life. 

Our target contribution to eSSIF-Lab is to reduce the barriers for developers (and the businesses they serve) via a reference implementation and advocate standards that be developed and adopted by various stakeholders. Thus, the SDKs, the reference implementation and the consent lifecycle design will be key assets for eSSIF-Lab stakeholders and subgrantees, as well as basis for interoperability work in the SSI ecosystem.

Project Partners:
unikk.me (DK)
iGrant.io (SE)

# Trusted Digital Agent (TDA) Delivery Summary

## Stage 01 deliveries

| **No.**           | **Date Delivered**     | **Document Delivered**    |
| :------------ | :----------------: | :--------------------- |
| Delivery 00   | 05-August-2020    | [Productisation Meeting Document](https://gitlab.grnet.gr/essif-lab/business/tda/deliverables/-/blob/master/00_productisation-meeting.md)     |
| Delivery 01   | 31-August-2020    | [Solution Description Document](https://gitlab.grnet.gr/essif-lab/business/tda/deliverables/-/blob/master/01_solution_description.md)     |
| Delivery 02   | 07-September-2020    | [Contribution to SSI and eSSIF-Lab vision and framework](https://gitlab.grnet.gr/essif-lab/business/tda/deliverables/-/blob/master/02_contributions_to_eSSIF-Lab.md)     |
| Delivery 03   | 14-September-2020    | [Collaboration within the SSI business ecosystem](https://gitlab.grnet.gr/essif-lab/business/tda/deliverables/-/blob/master/03_collaboration-with-ssi-ecosystem.md)    |

Furthermore, you can find a recording of our Stage 1 Hackaton Demo at https://www.youtube.com/watch?v=7W7jdy0z3pw

## Stage 02 deliveries

| **No.**          | **Document Delivered**    |
| :--------------- | :------------------------ | 
| Delivery 01.01   | [API Interface specification](https://gitlab.grnet.gr/essif-lab/business/tda/deliverables/-/blob/master/api_interface_specification_of_TDA_component.md), [Swagger specification](https://demo-aries-agent-admin.igrant.io/api/doc)  |
| Delivery 01.02   | [Functional specification](https://gitlab.grnet.gr/essif-lab/business/tda/deliverables/-/blob/master/functional_specification_of_TDA(Overall)_component.md), [FS: Aries playground](https://docs.igrant.io/ssi/ssi-apg), [FS: Aries Mobile Agent + Wallet](https://docs.igrant.io/ssi/ssi-ama)    |

# Our GitHub repositories

Our opensource code that is relevant for this project (SSI) is available at:
https://github.com/decentralised-dataexchange

The complete MyData Operator code from iGrant.io (Not open source) is at: https://github.com/L3-iGrant

The reference example which we use for all our demo can be found at: https://github.com/Data4Life-Initiative
